function getFanStatus() {
  return $.ajax({
    url: '/status',
    method: 'GET',
    dataType: 'json',
  });
}

function getConfig() {
  return $.ajax({
    url: '/config',
    method: 'GET',
    dataType: 'json',
  });
}

function updateConfig(request) {
  return $.ajax({
    url: '/config',
    method: 'POST',
    data: JSON.stringify(request),
    contentType: 'application/json',
    dataType: 'json',
  });
}
