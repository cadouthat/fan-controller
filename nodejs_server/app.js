const { PubSub } = require('@google-cloud/pubsub');
const smsDebug = require('debug')('fan-server:sms');
const tcpDebug = require('debug')('fan-server:tcp');
const express = require('express');
const fs = require('fs');
const path = require('path');
const net = require('net');
const twilio = require('twilio');

const DEFAULT_TCP_PORT = 8088;

const pubSubSubscriptionName = process.env.GOOGLE_PUBSUB_SUBSCRIPTION_NAME;
if (!pubSubSubscriptionName) {
  throw new Error('GOOGLE_PUBSUB_SUBSCRIPTION_NAME is required');
}

const twilioConfigPath = process.env.TWILIO_CONFIG_PATH;
if (!twilioConfigPath) {
  throw new Error('TWILIO_CONFIG_PATH is required');
}
const twilioConfig = JSON.parse(fs.readFileSync(twilioConfigPath));

let twilioClient = undefined;
function getTwilioClient() {
  if (!twilioClient) {
    twilioClient = twilio(twilioConfig.accountSid, twilioConfig.authToken);
  }
  return twilioClient;
}

const AlertType = {
  SERVER_STARTED: 'SERVER_STARTED',
  PUBSUB_ERROR: 'PUBSUB_ERROR',
  SENSOR_TIMEOUT: 'SENSOR_TIMEOUT',
};

async function sendAlertSms(type, message) {
  if (!config.alertsEnabled) {
    return;
  }

  const nowMs = new Date().getTime();
  if (nowMs - (config.lastAlertMsByType[type] || 0) < config.alertThrottleMs) {
    return;
  }
  config.lastAlertMsByType[type] = nowMs;
  storeConfig();

  if (!twilioConfig.fanAlertRecipients) {
    console.error(`No recipients for alert: "${message}"`);
    return;
  }

  for (const recipient of twilioConfig.fanAlertRecipients) {
    try {
      const response = await getTwilioClient().messages.create({
        body: message,
        to: recipient,
        from: twilioConfig.fromNumber,
      });
      smsDebug('SMS send success', response.sid);
    } catch (err) {
      status.smsErrorCount++;
      console.error('SMS send error', err);
    }
  }
}

function checkAlerts() {
  const nowMs = new Date().getTime();

  if (nowMs - status.serverStartMs > config.sensorTimeoutMs) {
    for (const localId of Object.keys(config.speedById)) {
      if (nowMs - (status.lastMessageMsById[localId] || 0) > config.sensorTimeoutMs) {
        sendAlertSms(AlertType.SENSOR_TIMEOUT, 'Sensor timeout on fan server');
        return;
      }
    }
  }
}

const config = {
  speedById: {},
  fanPower: 'on/off/auto',
  alertsEnabled: true,
  alertThrottleMs: 30 * 60 * 1000,
  sensorTimeoutMs: 10 * 60 * 1000,
  lastAlertMsByType: {},
};

function updateConfig(changes) {
  for (const [key, val] of Object.entries(changes)) {
    if (!config.hasOwnProperty(key) || typeof config[key] != typeof val) {
      throw new Error(`invalid config ${key}`);
    }
  }

  for (const [key, val] of Object.entries(changes)) {
    config[key] = val;
  }

  storeConfig();
}

function storeConfig() {
  if (!process.env.CONFIG_PATH) {
    return;
  }
  
  fs.writeFileSync(process.env.CONFIG_PATH, JSON.stringify(config, null, 2));
}

function loadConfig() {
  if (!process.env.CONFIG_PATH || !fs.existsSync(process.env.CONFIG_PATH)) {
    return;
  }

  const savedConfig = JSON.parse(fs.readFileSync(process.env.CONFIG_PATH));

  updateConfig(savedConfig);
}

const socketById = {};
const status = {
  serverStartMs: new Date().getTime(),
  lastMessageMsById: {},
  isCooling: false,
  smsErrorCount: 0,
  hvacTimestamp: new Date(0),
  pubsubLog: [],
};

const getFansEnabled = () => {
  if (config.fanPower === 'on') {
    return true;
  }
  if (config.fanPower === 'off') {
    return false;
  }
  return status.isCooling;
}

const sendSpeed = (localId) => {
  const socket = socketById[localId];
  if (!socket) {
    return;
  }

  const speed = (getFansEnabled() && config.speedById[localId]) || 0;

  socket.write(new Uint8Array([speed]));
};

const sendAllSpeeds = () => {
  for (const localId of Object.keys(socketById)) {
    sendSpeed(localId);
  }
};

const setIsCooling = (isCooling) => {
  status.isCooling = isCooling;

  sendAllSpeeds();
}

const fanTcpServer = net.createServer((socket) => {
  tcpDebug('Socket connected');

  let localId = '';

  socket.on('data', (data) => {
    if (data.length != 1) {
      tcpDebug('Bad message', localId);
      socket.destroy();
      return;
    }

    const updatedId = String(data[0]);
    if (localId != updatedId) {
      tcpDebug('ID Update', localId, updatedId);
      localId = updatedId;

      const existing = socketById[localId];
      if (existing && existing != socket) {
        existing.destroy();
      }

      socketById[localId] = socket;

      if (!config.speedById[localId]) {
        config.speedById[localId] = 0;
        storeConfig();
      }
    }

    status.lastMessageMsById[localId] = new Date().getTime();

    sendSpeed(localId);
  });
  
  socket.on('close', () => {
    tcpDebug('Connection closed', localId);

    if (localId && socketById[localId] == socket) {
      delete socketById[localId];
    }
  });

  socket.on('error', (err) => {
    tcpDebug(err);
  });
});

fanTcpServer.on('error', (err) => {
  console.error(err);
  process.exit(1);
});

fanTcpServer.on('listening', () => {
  tcpDebug('Listening on', fanTcpServer.address());
})

fanTcpServer.listen(parseInt(process.env.FAN_PORT) || DEFAULT_TCP_PORT, '0.0.0.0');

const app = express();

app.use(express.json());

app.use(express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, 'public/index.html')));

app.get('/status', (_req, res) => {
  res.json({ ...status, fansEnabled: getFansEnabled() });
});

app.get('/config', (_req, res) => {
  res.json(config);
});

app.post('/config', (req, res) => {
  updateConfig(req.body);

  sendAllSpeeds();

  res.json(config);
});

const pubSubClient = new PubSub();
const subscription = pubSubClient.subscription(pubSubSubscriptionName);

subscription.on('message', (message) => {
  message.ack();

  const data = JSON.parse(message.data.toString());
  const hvacStatus = data?.resourceUpdate?.traits?.['sdm.devices.traits.ThermostatHvac']?.status;

  while (status.pubsubLog.length > 20) {
    status.pubsubLog.shift();
  }
  status.pubsubLog.push(data);

  const timestamp = new Date(data.timestamp);
  if (hvacStatus && timestamp > status.hvacTimestamp) {
    status.hvacTimestamp = timestamp;

    setIsCooling(hvacStatus == 'COOLING');
  }
});

subscription.on('error', (err) => {
  console.error('PubSub error', err);

  sendAlertSms(AlertType.PUBSUB_ERROR, 'PubSub error on fan server');

  subscription.close();
});

subscription.on('close', () => {
  console.error('PubSub closed');

  setTimeout(() => subscription.open(), 60000);
});

loadConfig();

sendAlertSms(AlertType.SERVER_STARTED, 'Fan server started');

setInterval(checkAlerts, 10000);

module.exports = app;
