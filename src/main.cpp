#include <stdio.h>

#include "hardware/pwm.h"
#include "pico/stdlib.h"
#include "pico/cyw43_arch.h"
#include "pico/time.h"

#include "lwip/dns.h"
#include "lwip/pbuf.h"
#include "lwip/tcp.h"

#include "pico_wifi_boot/flash.h"
#include "pico_wifi_boot/wifi_manager.h"

constexpr uint PWM_GPIO = 15;
constexpr const char *SERVER_HOSTNAME = "fans.lan";
constexpr uint16_t SERVER_PORT = 8088;
constexpr int POLL_INTERVAL_S = 60;
constexpr int CONNECTION_TIMEOUT_S = 180;
constexpr int WIFI_TIMEOUT_S = 400;

struct __attribute__((__packed__)) FlashConfigValues {
  uint8_t local_id;
};

struct __attribute__((__packed__)) RequestPayload {
  uint8_t local_id;
};

struct __attribute__((__packed__)) ResponsePayload {
  uint8_t fan_speed;
};

struct FlashConfigValues flash_config;
uint32_t last_updated_ms = 0;
struct tcp_pcb *active_pcb = nullptr;

bool wifi_init() {
  if (cyw43_arch_init() != 0) {
    printf("cyw43 init failed\n");
    return false;
  }

  sleep_ms(2000);
  printf("Press enter to configure wifi.. ");
  int entry;
  while (!(entry = getchar_timeout_us(5000 * 1000))) {
    printf("NULL entry\n");
    sleep_ms(100);
  }
  printf("\n");

  if (entry == '\r') {
    attempt_wifi_configure();
  }

  while (!wifi_connect(3)) {
    cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
    attempt_wifi_configure();
  }

  cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);

  return true;
}

bool wifi_reconnect() {
  printf("Disabling cyw43 before reconnect\n");
  cyw43_arch_disable_sta_mode();
  sleep_ms(5000);

  printf("Starting reconnect attempts\n");
  return wifi_connect(INT_MAX);
}

void send_request() {
  struct RequestPayload request;
  request.local_id = flash_config.local_id;
  err_t err = tcp_write(active_pcb, &request, sizeof(request), TCP_WRITE_FLAG_COPY);
  if (err != ERR_OK) {
    printf("failed to write data, err %d\n", err);
    return;
  }

  tcp_output(active_pcb);
}

void process_response(struct ResponsePayload response) {
  last_updated_ms = to_ms_since_boot(get_absolute_time());

  printf("response %d\n", (int)response.fan_speed);

  pwm_set_gpio_level(PWM_GPIO, response.fan_speed);
}

static err_t tcp_client_connected(void *arg, struct tcp_pcb *tpcb, err_t err) {
  printf("connected\n");
  send_request();
  return ERR_OK;
}

err_t tcp_client_recv(void *arg, struct tcp_pcb *tpcb, struct pbuf *p, err_t err) {
  struct ResponsePayload response;
  if (!p || p->tot_len != sizeof(response)) {
    if (p) {
      // TODO: handle potential message fragmentation or grouping
      printf("got unexpected payload size\n");
      pbuf_free(p);
    } else {
      printf("server closed connection\n");
    }

    active_pcb = nullptr;
    tcp_abort(tpcb);
    return ERR_ABRT;
  }

  pbuf_copy_partial(p, &response, sizeof(response), 0);

  process_response(response);

  tcp_recved(tpcb, p->tot_len);
  pbuf_free(p);

  return ERR_OK;
}

static err_t tcp_client_poll(void *arg, struct tcp_pcb *tpcb) {
  if (to_ms_since_boot(get_absolute_time()) - last_updated_ms >= CONNECTION_TIMEOUT_S * 1000) {
    printf("connection timed out, closing\n");
    tcp_abort(tpcb);
    active_pcb = nullptr;
    return ERR_ABRT;
  }

  send_request();
  return ERR_OK;
}

static void tcp_client_err(void *arg, err_t err) {
  printf("tcp_client_err %d\n", err);
  active_pcb = nullptr;
}

void attempt_connect(ip_addr_t addr) {
  if (active_pcb) {
    printf("refusing to connect while PCB is active\n");
    return;
  }

  printf("attempt_connect\n");

  active_pcb = tcp_new_ip_type(IP_GET_TYPE(&addr));
  if (!active_pcb) {
    printf("failed to create pcb\n");
    return;
  }

  tcp_poll(active_pcb, tcp_client_poll, POLL_INTERVAL_S * 2);
  tcp_recv(active_pcb, tcp_client_recv);
  tcp_err(active_pcb, tcp_client_err);

  cyw43_arch_lwip_begin();
  err_t err = tcp_connect(active_pcb, &addr, SERVER_PORT, tcp_client_connected);
  cyw43_arch_lwip_end();

  if (err != ERR_OK) {
    printf("tcp_connect failed %d\n", (int)err);
  }
}

static void dns_found(const char *hostname, const ip_addr_t *ipaddr, void *arg) {
  if (ipaddr) {
    printf("dns lookup success\n");
    attempt_connect(*ipaddr);
  } else {
    printf("dns request failed\n");
  }
}

void begin_connect_lookup() {
  printf("begin_connect_lookup\n");

  cyw43_arch_lwip_begin();
  ip_addr_t server_address;
  int err = dns_gethostbyname(SERVER_HOSTNAME, &server_address, dns_found, nullptr);
  cyw43_arch_lwip_end();

  if (err == ERR_OK) {
    attempt_connect(server_address);
  } else if (err != ERR_INPROGRESS) {
    printf("dns request failed %d\n", (int)err);
  }
}

int main() {
  stdio_init_all();
  sleep_ms(2500);

  printf("Initializing PWM GPIO\n");
  gpio_set_function(PWM_GPIO, GPIO_FUNC_PWM);
  pwm_set_gpio_level(PWM_GPIO, 0);

  uint pwm_slice = pwm_gpio_to_slice_num(PWM_GPIO);
  pwm_config pwm_config = pwm_get_default_config();
  pwm_config_set_wrap(&pwm_config, UINT8_MAX - 1);
  pwm_init(pwm_slice, &pwm_config, true);

  printf("Initializing wifi\n");
  if (!wifi_init()) {
    while (1) {
      tight_loop_contents();
    }
  }

  if (!read_flash_config_extra(&flash_config, sizeof(flash_config))) {
    flash_config.local_id = to_ms_since_boot(get_absolute_time()) % UINT8_MAX;

    printf("generated a new local ID %d\n", (int)flash_config.local_id);

    if (!write_flash_config_extra(&flash_config, sizeof(flash_config))) {
      printf("Failed to write flash config!\n");
      while (1) {
        tight_loop_contents();
      }
    }
  }

  printf("Local ID %d\n", (int)flash_config.local_id);

  uint32_t last_attempt = 0;
  last_updated_ms = to_ms_since_boot(get_absolute_time());
  uint32_t last_reconnect_ms = 0;
  while (1) {
    sleep_ms(10);

    cyw43_arch_poll();

    uint32_t now_ms = to_ms_since_boot(get_absolute_time());
    if (now_ms - MAX(last_updated_ms, last_reconnect_ms) > WIFI_TIMEOUT_S * 1000) {
      last_reconnect_ms = now_ms;
      wifi_reconnect();
    }

    if (!active_pcb && (!last_attempt || now_ms - last_attempt >= POLL_INTERVAL_S * 1000)) {
      begin_connect_lookup();
      last_attempt = now_ms;
    }
  }
}
